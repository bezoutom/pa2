#include <iostream>
#include "complex.hpp"

using namespace std;

int main() 
{
  Complex x(1,1), y(2,2), z;


  const Complex pi(1,1);

  cout << pi.Abs();

  float a;

  cout << "x = " << x.Print() << endl;
  cout << "y = " << y.Print() << endl;

  z = Add(x, y);

  cout << "x + y = " << z.Print() << endl;

  z = x.Diff(y);

  cout << "x - y = " << z.Print() << endl;

  a = x.Abs();

  cout << "abs(x) = " << a << endl;

  return 0;
}