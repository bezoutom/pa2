#ifndef __COMPLEX_HPP_JLHFLJKDALKJKJLKDJLKN
#define __COMPLEX_HPP_JLHFLJKDALKJKJLKDJLKN 

#include <string>

class Complex
{
private:
  float r;
  float i;

  void Init(float, float);
public:
  Complex(float, float);
  Complex();
  float Abs() const;
  std::string Print() const;

  float GetReal() const;
  float GetImg() const;

  Complex Add(Complex) const;
  Complex Diff(Complex) const;
};

Complex Add(Complex, Complex);
Complex Diff(Complex, Complex);

#endif