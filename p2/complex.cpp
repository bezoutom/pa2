#include <sstream>
#include <cmath>
#include "complex.hpp"

using namespace std;

void Complex::Init(float r, float i)
{
  this->r = r;
  this->i = i;
}

Complex::Complex(float r, float i)
{
  Init(r, i);
}

Complex::Complex()
{
  Init(0, 0);
}

float Complex::Abs() const
{
  return sqrt(r * r + i * i);
}
  
string Complex::Print() const
{
  stringstream ss;
  ss << r << ((i < 0) ? " - " : " + ") << abs(i) << "i";
  return ss.str();
}

float Complex::GetReal() const
{
  return r;
}

float Complex::GetImg() const
{
  return i;
}

Complex Complex::Add(Complex other) const
{
  return Complex(r + other.r, i + other.i); 
}

Complex Complex::Diff(Complex other) const
{
  return Complex(r - other.r, i - other.i); 
}

Complex Add(Complex a, Complex b)
{
  Complex c(a.GetReal() + b.GetReal(), a.GetImg() + b.GetImg());

  return c;
}

Complex Diff(Complex a, Complex b)
{
  return Complex(a.GetReal() - b.GetReal(), a.GetImg() - b.GetImg());
}
