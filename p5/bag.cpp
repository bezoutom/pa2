#include "bag.hpp"
#include <iostream>

using namespace std;

Bag::Bag(int _size) : count(0), size(_size), items(new int[_size])
{    
}

Bag::Bag(const Bag & bag)
{
  CopyObject(bag);
}

Bag & Bag::operator=(const Bag & bag)
{
  if (this != &bag) 
  {
    Delete();
    CopyObject(bag);
  }
  return *this;
}

void Bag::CopyObject(const Bag & bag)
{
  size = bag.size;
  count = bag.count;
  items = new int[size];
  for (int i = 0; i < count; ++i)
  {
    items[i] = bag.items[i];
  }
}

Bag::~Bag()
{    
  cout << "Dtor called " << items << endl;
  Delete();  
}

void Bag::Delete()
{  
  delete [] items;
  items = NULL;
}

void Bag::Insert(int item)
{
  items[count++] = item;
}

void Bag::Remove(int item)
{
  int i = 0;
  for ( ; i < count; ++i)
  {
    if (items[i] == item) break;
  }
  if (i != count) count--; // if found
  for (int i = 0; i < count; ++i)
  {
    items[i] = items[i+1];
  }
}

bool Bag::IsSet(int item) const
{
  for (int i = 0; i < count; ++i)
  {
    if (items[i] == item) return true;
  }
  return false;
}

Bag& Bag::operator+=(int item)
{
  Insert(item);
  return *this;
}
  
ostream& operator<<(ostream& os, const Bag & bag)
{  
  for (int i = 0; i < bag.count; ++i)
  {
    os << bag.items[i] << " ";
  }
  return os;
}