#include <iostream>
#include "bag.hpp"

using namespace std;


// void foo(const Bag & bag)
// {
//   bag += 42;
//   bag.Remove(2);
//   cout << bag << endl;
// }

// void foo(Bag & bag)
// {
//   bag += 42;
//   bag.Remove(2);
//   cout << bag << endl;
// }

void foo(Bag bag)
{
  bag += 42;
  bag.Remove(2);
  cout << bag << endl;
}

int main() 
{
  Bag b1(100);



  b1.Insert(1);
  b1.Insert(3);
  b1.Insert(1);
  b1.Insert(2);
  cout << "b1 = " << b1 << endl;


  foo(b1);
  cout << b1 << endl;
  b1.Insert(2);

  Bag b2 = b1;
  b1.Remove(1);
  cout << "b1 = " << b1 << endl;
  cout << "b2 = " << b2 << endl;
  Bag b3(100);
  b3 = b1;
  b1.Remove(1);
  cout << "b1 = " << b1 << endl;
  cout << "b3 = " << b3 << endl;

  return 0;
}