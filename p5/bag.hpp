#include <iostream>

class Bag
{
private:
  int * items;
  int count;
  int size;

  void CopyObject(const Bag & bag);
  void Delete();
public:
  Bag(int);  
  Bag(const Bag & bag);
  ~Bag();

  void Insert(int);
  void Remove(int);
  bool IsSet(int) const;
  
  Bag & operator+=(int);
  Bag & operator=(const Bag &);
  friend std::ostream & operator<<(std::ostream &, const Bag &);
    
};