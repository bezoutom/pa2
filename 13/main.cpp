#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;


class Item
{
public:
  virtual string GetName() const = 0;
  virtual double GetPrice() const = 0;

  virtual ~Item() {}
};

class BasicItem : public Item
{
private:
  string name;
  double price;
public:
  BasicItem(string name, double price) : name(name), price(price) {}

  virtual string GetName() const { return name; }
  virtual double GetPrice() const { return price; }
};

class VegetableItem : public BasicItem
{
private:
  string name;
  double price;
public:
  BasicItem(string name, double price) : name(name), price(price) {}

  virtual string GetName() const { return name; }
  virtual double GetPrice() const { return price; }
};

class CrazyItem : public Item
{
private:
  string name;
public:
  CrazyItem(string name) : name(name) {}

  virtual string GetName() const { return name; }
  virtual double GetPrice() const { return rand() % 1000 + 1; }
};


class Collection : private vector<Item*>
{
public:
  using vector<Item*>::push_back;

  virtual double GetPrice() const 
  {
    double cntr = 0;  
    for (typename vector<Item*>::const_iterator iter = this->begin(); iter != this->end(); ++iter)
    {
      cntr += (*iter)->GetPrice(); // requires "T" to be "Item"
    }
    return cntr;
  }
};

class Bag : public Collection, public Item
{

public:
  virtual string GetName() const { return "Taska"; }
  virtual double GetPrice() const { return 5 + Collection::GetPrice(); }
};



int main()
{

  Collection trolley;

  trolley.push_back(new CrazyItem("Crazy frog"));
  trolley.push_back(new BasicItem("Chocolate", 42));

  Bag bag;
  bag.push_back(new BasicItem("bread", 10));
  bag.push_back(new BasicItem("milk", 15));
  bag.push_back(new BasicItem("cheese", 12));

  Bag bag2;
  bag2.push_back(new BasicItem("bread", 10));
  bag2.push_back(new BasicItem("milk", 15));
  bag2.push_back(new BasicItem("cheese", 12));
  bag.push_back(&bag2);

  // cout << bag.GetPrice(); // ambiguous 
  // cout << bag.Collection::GetPrice(); // solution for ambiguosity

  trolley.push_back(&bag);

  cout << trolley.GetPrice() << endl;

  vector<Item> items;

  items.push_back(CrazyItem("sss"))




  return 0;
}