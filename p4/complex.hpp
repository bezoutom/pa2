#ifndef __COMPLEX_HPP_JLHFLJKDALKJKJLKDJLKN
#define __COMPLEX_HPP_JLHFLJKDALKJKJLKDJLKN 

#include <string>
#include <iostream>


class Complex
{
private:
  float r;
  float i;

  void Init(float, float);
public:
  Complex(float, float);
  Complex(float);
  Complex();
  float Abs() const;
  std::string Print() const;

  float GetReal() const;
  float GetImg() const;


  friend std::ostream& operator<<(std::ostream &, const Complex &);
  friend std::istream& operator>>(std::istream &, Complex &);

  friend Complex operator+(const Complex &, const Complex &);
  friend Complex operator-(const Complex &, const Complex &);

  friend bool operator==(const Complex &, const Complex &);
  friend bool operator!=(const Complex &, const Complex &);
  
  Complex operator++(int); // post
  Complex& operator++(); // pre

};


#endif