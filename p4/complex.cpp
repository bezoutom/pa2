#include <sstream>
#include <cmath>
#include "complex.hpp"

using namespace std;

void Complex::Init(float r, float i)
{
  this->r = r;
  this->i = i;
}

Complex::Complex(float r, float i)
{
  Init(r, i);
}

Complex::Complex(float r)
{
  Init(r, 0);
}

Complex::Complex()
{
  Init(0, 0);
}

float Complex::Abs() const
{
  return sqrt(r * r + i * i);
}
  
string Complex::Print() const
{
  stringstream ss;
  ss << r << ((i < 0) ? " - " : " + ") << abs(i) << "i";
  return ss.str();
}

float Complex::GetReal() const
{
  return r;
}

float Complex::GetImg() const
{
  return i;
}

std::ostream& operator<<(std::ostream & os, const Complex & c)
{
  os << c.Print();

  return os;
}

std::istream& operator>>(std::istream &in, Complex & c) { //3 + 3i
  char i, a;
  in >> c.r >> i >> c.i >> a;

  if (i == '-') {
    c.i = -c.i;
  }

  if (a != 'i') {
    in.setstate(std::ios::failbit);
  }

  return in;
}

Complex operator+(const Complex & a, const Complex & c)
{
  return Complex(a.r + c.r, a.i + c.i);
}

Complex operator-(const Complex & a, const Complex & c)
{
  return Complex(a.r - c.r, a.i - c.i);
}

bool operator==(const Complex & a, const Complex & b)
{
  return a.r == b.r && a.i == b.i;
}

bool operator!=(const Complex & a, const Complex & b)
{
  return !(a == b);
}

Complex& Complex::operator++() 
{
    r++;
    return *this;
}

Complex Complex::operator++(int)
{
    Complex tmp(r, i); // copy
    operator++(); // pre-increment
    return tmp;   // return old value
}


