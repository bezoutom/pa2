#ifndef VEHICLE__LKDJFLJDFKJSDLKFJLJ
#define VEHICLE__LKDJFLJDFKJSDLKFJLJ

#include <string>
#include <iostream>

class Vehicle
{
private:
  std::string plate_num;
  int year;
protected:
  int speed;  
public:
  Vehicle(std::string plate_num, int year);
  void GoVeyron(); // v
  virtual void GoFaster();
  void GoOk();
  virtual void Print(std::ostream &) const;
  virtual void Print() const; // cout

  Vehicle(const Vehicle & v)
  {
    std::cout << "copied" << std::endl;
  }

  friend std::ostream& operator<<(std::ostream&, const Vehicle &);
};

#endif