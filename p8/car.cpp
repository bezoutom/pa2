#include "car.hpp"

using namespace std;

Car::Car(std::string plate_num, int year, int seats) 
: Vehicle(plate_num, year), seats(seats) 
{
  cout << "Car ctored" << endl;
}

void Car::GoVeyron()
{
  speed = 450;
}

void Car::GoFaster()
{
  speed += 30;
}

void Car::Print(ostream & os) const
{
  Vehicle::Print(os); // nezbyva nic jineho, plate_num a year jsou private
  os << endl << "Seats :" << seats << endl;
}