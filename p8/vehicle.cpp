#include "vehicle.hpp"

using namespace std;

Vehicle::Vehicle(string plate_num, int year) 
: plate_num(plate_num), year(year) 
{
  GoOk();
  cout << "Vehicle ctored" << endl;
}

void Vehicle::GoVeyron()
{
  speed = 400;
}

void Vehicle::GoFaster()
{
  speed += 10;
}

void Vehicle::GoOk()
{
  speed = 50;
}

void Vehicle::Print(std::ostream & os) const
{
os << "Vehicle : " << plate_num << endl 
     << "Year : " << year << endl 
     << "Speed : " << speed;
}

void Vehicle::Print() const
{
  Print(cout);
}

ostream& operator<<(ostream& os, const Vehicle & v)
{
  v.Print(os);
  return os;
}







