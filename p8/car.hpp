#ifndef CAR_HPP_LKDJFLJSDLKFJKJSDBGLKJLJEOIR
#define CAR_HPP_LKDJFLJSDLKFJKJSDBGLKJLJEOIR

#include "vehicle.hpp"


class Car : public Vehicle
{
private:
  int seats;
public:
  Car(std::string plate_num, int year, int seats);
  void GoVeyron(); //v
  virtual void GoFaster();
  virtual void Print(std::ostream &) const;


  using Vehicle::Print;
};

#endif