#include "vehicle.hpp"
#include "car.hpp"
#include <iostream>
#include <vector>
using
 namespace std;


void SetSpeedFaster(Vehicle & vehicle)
{
  vehicle.GoFaster();
}

void SetSpeedVeyron(Vehicle & vehicle)
{
  vehicle.GoVeyron();
}

void Vypis(Vehicle & v)
{
  cout << v << endl;
}

int main()
{
  
  Vehicle v("1A1 1111", 2000);
  SetSpeedFaster(v); 
  cout << v << endl << endl; // Vypis(v);

  Car c("1A1 1112", 2001, 5);

  SetSpeedFaster(c); 
  cout << c << endl << endl; // Vypis(c);
  
  SetSpeedVeyron(c); 
  Vypis(c);
  

  c.Print();


  // pouzije se Copy ctor!!
  // vector<Vehicle> vs;
  // vs.push_back(c);
  // cout << (vs[0]) << endl;


  vector<Vehicle*> vs;
  vs.push_back(&c);
  cout << *(vs[0]) << endl;


  // vector<Vehicle*> vehicles2;
  // vehicles2.push_back(&v);
  // vehicles2.push_back(&c);

  return 0;
}